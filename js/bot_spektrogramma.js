var oData,
    response_server,
    dialogue_id,
    oReq,
    audioContext,
    timer_,
    audio_array = [],
    audio_item = 0,
    wavesurfer,
    wave_height,
    cb_bot;

let cb_block_recorder_top = false;

var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

var recorder;
var microphone;
var microphone_;

const IMGSRC = "images/"


    class cb {
        constructor() {
            this.mobile_width = 800;
            this.create_launch();
            this.create_modal_conversation();
        }

        create_launch() {
            $('body').append('<div class="widget_mozg_launcher"></div>');

            this.create_launch_desktop();
            this.create_launch_mobile();
            this.start_launch();
        }

        create_modal_conversation() {
            $('body').append(`
                <div class="widget_mozg">
                    <div class="widget_mozg_conversation js_widget_open">
                        <div class=" widget_mozg_wrapper">
                            <div class="widget_mozg_top">
                                <div class="widget_mozg_top_title">Спектрограмма</div>
                                <div class="widget_mozg_top_close js_widget_mozg_top_close"></div>
                            </div>

                            <div class="widget_mozg_main"></div>
                            <div class="widget_mozg_bottom hide--before__converstion js_widget_mozg_bottom">

                                <form action="javascript:void(0)" class="js_bot_message_form">
                                    <input type="text" name="message" placeholder="Или напишите текстом, если есть вопросы" class="widget_mozg_text_field js_widget_mozg_text_field">
                                    <button type="submit" class="js_widget_mozg_btn_send widget_mozg_btn_send cb_btn_deactive"></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>`);

            document.querySelector('.js_bot_message_form').addEventListener("submit", () => {
                var user_text = $('.js_widget_mozg_text_field').val();
                $('.js_widget_mozg_text_field').val('');
            	this.bot_message_user(user_text);
                this.send_server(user_text,'text')
            })

            document.querySelector('.widget_mozg_text_field').addEventListener("keyup", () => {
                if (($('.js_widget_mozg_text_field').val().trim().length) == 0) {
                    $('.js_widget_mozg_btn_send').addClass('cb_btn_deactive');
                } else {
                    $('.js_widget_mozg_btn_send').removeClass('cb_btn_deactive');

                }
            })

            document.querySelector('.js_widget_mozg_top_close').addEventListener("click", () => {
                this.close_conversation_modal();
            })
            this.create_conversation_hello();

            $('.widget_mozg_main').append('<div class="widget_mozg_main_conversation"></div>');
        }

        create_conversation_hello() {








            $('.widget_mozg_main').append(`
                <img src="${IMGSRC}botHello.png" class='botHello__image js_botHello__image'>
                <div class="widget_mozg_main_desktop_hello">

                    <div class="widget_mozg_main_desktop_hello_left">

                        <div class="widget_mozg_main_desktop_hello_title">
                            Здравствуйте!
                        </div>

                        <div class="widget_mozg_main_desktop_hello_left_wrap">
                            <p>Это Спектограмма — специальный виджет, который помогает проверять уровень развития устной речи у детей от 2 до 6 лет.</p>

                            <p>Я буду отправлять ребёнку задания, подобранные специально для его возраста. Наши специалисты проанализируют запись ответов и определят возможные дефекты речи, если они есть. Также мы дадим вам рекомендации по посещению специалиста, если вдруг это понадобится.</p>
                        </div>

                        <div class="widget_mozg_main_desktop_hello_left_small_text">
                            <div class="widget_mozg_main_desktop_hello_left_small_text__wrap">
                                <svg width="22" height="22" viewBox="0 0 22 22">
                                    <path id="Path_2" data-name="Path 2" d="M16,5A11,11,0,1,0,27,16,11,11,0,0,0,16,5Zm1.843,16.152a5.511,5.511,0,0,1-1.923,1.823,3.323,3.323,0,0,1-1.567.335c-1.421-.118-2.081-1.189-1.461-2.892l1.868-5.133c.4-1.108.012-1.445-.308-1.453q-.571-.011-1.365,1.07a.282.282,0,0,1-.46-.326,5.513,5.513,0,0,1,1.923-1.823,3.32,3.32,0,0,1,1.567-.335c1.421.118,2.081,1.188,1.461,2.892l-1.868,5.133c-.4,1.108-.012,1.444.308,1.453q.571.011,1.365-1.07a.282.282,0,0,1,.46.326Zm1.5-10.685a1.329,1.329,0,1,1-.794-1.7,1.329,1.329,0,0,1,.794,1.7Z" transform="translate(-5 -5)" opacity="0.3"/>
                                </svg>

                                <div class="widget_mozg_main_desktop_hello_left_small_text__t">
                                    Мы не будем хранить никаких личных данных! У нас одна цель — помочь гармоничному развитию вашего ребёнка. <br>
                                    Продолжая тестирование, вы соглашаетесь с правилами использования сервиса.
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="widget_mozg_main_desktop_hello_right">
                        <div class="widget_mozg_main_desktop_hello__right__inner">
                            <div class="widget_mozg_main_desktop_hello__right__inner_x">

                                <div class="widget_mozg_main_desktop_hello_title--2">
                                    Перед тем, как приступить...
                                </div>

                                <div class="widget_mozg_main_desktop_hello_right_wrap">
                                    <p>
                                        <span class='widget_mozg--color_orange'>1.</span>
                                        <b>Проверьте, что микрофон вашего компьютера или гарнитуры работает.</b>
                                        <div class="widget_mozg_main_desktop_hello_microphone">
                                            <div class="widget_mozg_main_desktop_hello_microphone_t">
                                                Понять это можно по индикатору снизу. Если вы говорите и индикатор меняется, значит, всё в порядке.
                                            </div>

                                            <div class="widget_mozg_main_desktop_hello_microphone_view__line">
                                                <div class="widget_mozg_main_desktop_hello_microphone_view">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>

                                                <a href="">У меня не работает микрофон.</a>
                                            </div>
                                        </div>
                                    </p>

                                    <div class="widget_mozg_main_desktop_hello__wrapp">
                                        <p>
                                            <span class='widget_mozg--color_orange'>2.</span>
                                            <b>Пожалуйста, позовите вашего ребёнка к экрану. Ему нужно будет вслух отвечать на задания.</b>
                                        </p>

                                        <p>
                                            <span class='widget_mozg--color_orange'>3.</span>
                                            <b>Следуйте нашим инструкциям! :)</b>
                                            Если вдруг ребёнок не поймёт наши задания, помогите ему разобраться или прочитайте их вслух. Мы обещаем: ничего сложного не будет!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget_mozg_main_desktop_hello_line">
                        <img src="${IMGSRC}botHello.png" class='botHello__image--mobile'>
                        <button class="js_btn_cb_bot btn_cb_bot">
                            <span class="btn_cb_bot__inner--small">Запустить тестироваие</span>
                            <span class="btn_cb_bot__inner">Мы готовы. Давайте приступать к тестированию!</span>
                        </button>
                    </div>
                </div>`);
            document.querySelector('.js_btn_cb_bot').addEventListener("click", () => {
                $('.widget_mozg_main_desktop_hello_left').addClass('widget_mozg_main_desktop_hello_left_hide');
                $('.widget_mozg_main_desktop_hello_right').addClass('widget_mozg_main_desktop_hello_right_hide');
                $('.js_botHello__image').addClass('botHello__image__hide');
                $('.js_widget_mozg_bottom').removeClass('hide--before__converstion');

                setTimeout(() => $('.widget_mozg_main_desktop_hello').hide(0), 500);
                this.bot_do();
               microphone_.mediaStream.stop();
            })

        }
        check_microphone() {
            _check_microphone();
        }

        create_launch_desktop() {
            $('.widget_mozg_launcher').append('<div class="widget_mozg_launcher_desktop"><div class="widget_mozg_launcher_desktop_wrap"><div class="js_widget_mozg_launcher_desktop_close widget_mozg_launcher_desktop_close"></div><div class="widget_mozg_launcher_desktop_text">Хотите узнать про раннее диагностирование дефектов речи у детей?</div><div class="widget_mozg_launcher_desktop_btn js_go_conversstion">Запустить тест</div></div></div>');
            document.querySelector('.js_go_conversstion').addEventListener("click", () => {
                this.close_desktop_launch();
                this.open_conversation_modal();
            })
            document.querySelector('.js_widget_mozg_launcher_desktop_close').addEventListener("click", () => {
                this.close_desktop_launch();
            })
        }

        create_launch_mobile() {
            $('.widget_mozg_launcher').append(`
                <div class="widget_mozg_launcher_small js_widget_mozg_launcher_small">
                    <div class="widget_mozg_launcher_small__text">Проверьте, насколько развита речь вашего ребёнка.
                    Пройдите специальный тест — мы поможем диагностировать дефекты и расскажем, как их исправить.</div>
                    <div class="widget_mozg_launcher_bg"></div>
                    <div class="launch_bot__animate"></div>
                </div>`);
            let launch_btn = document.querySelector('.js_widget_mozg_launcher_small');
            launch_btn.addEventListener("click", () => {
                if (launch_btn.classList.contains('cb_small_opened')) {
                    this.close_conversation_modal();
                } else {
                    this.open_conversation_modal();
                }
            })
        }
        start_launch() {
            this.show_small_launch();
            // $(".widget_mozg_launcher_desktop").addClass('cb_open_desktop');
        }

        scroll_bottom(){
                $(".widget_mozg_main_conversation").animate({
                  scrollTop: $('.widget_mozg_main_conversation')[0].scrollHeight
                }, 700)

        }


        show_small_launch() {
            setTimeout(() => $(".widget_mozg_launcher_small").addClass('cb_small_visible'), 500);
        }


        close_desktop_launch() {
            $(".widget_mozg_launcher_desktop").removeClass('cb_open_desktop');
            this.show_small_launch();
        }

        open_conversation_modal() {
            $('.js_widget_open').addClass('conversation_modal_opened');

            $('.widget_mozg_top').addClass('bounce_animate__bottom');
            $('.js_botHello__image').addClass('bounce_animate__right');
            $('.widget_mozg_main_desktop_hello').addClass('bounce_animate__top');

            $('.js_widget_mozg_launcher_small .widget_mozg_launcher_small__text').fadeOut(400)


            $(".widget_mozg_launcher_small").addClass('cb_small_opened');
            this.check_microphone();
        }
        close_conversation_modal() {
            $('.js_widget_open').removeClass('conversation_modal_opened');
            $('.widget_mozg_top').removeClass('bounce_animate__bottom');
            $('.js_botHello__image').removeClass('bounce_animate__right');
            $('.widget_mozg_main_desktop_hello').removeClass('bounce_animate__top');


            $(".widget_mozg_launcher_small").removeClass('cb_small_opened');
        }
        bot_do() {
            if (!$('.widget_mozg_main_conversation').hasClass('js_bot_started')) {
                $('.widget_mozg_main_conversation').addClass('js_bot_started');
                this.send_server('','')
            }
        }

        send_server(send_elem,type_send_elem){
            oData = new FormData()


            if (dialogue_id != undefined) {
                oData.append( "dialogue_id", dialogue_id);
                if (type_send_elem =="text")
                    oData.append( "text", send_elem);
                if (type_send_elem =="attachment")
                    oData.append( "attachment", send_elem);
            }

            oReq = new XMLHttpRequest();
            oReq.open("POST", "https://spectrogramma.cleverbots.ru/api/handle/");
            oReq.send(oData);

            oReq.onload = function () {
                if (oReq.readyState === oReq.DONE) {
                    if (oReq.status === 200) {
                        response_server = JSON.parse(oReq.response);
                        console.log(response_server);
                        var message_with_avatar,time_message;
                        var events = response_server.events;
                        var count_bot_message = 0;
                        let no_scroll = false;
                        for (let i = 0; i < events.length; i++) {
                            let timeDelay = 0;

                            if (events[i].event_type == "message"){

                               let time_message = (count_bot_message == events.length-1) ? true : false;

                               setTimeout((e) => {
                                    cb_bot.bot_message(events[i].message.text, message_with_avatar,time_message);
                               },  count_bot_message*500);


                               if (events[i].message.attachment!=undefined)

                                    setTimeout((e) => {
                                        cb_bot.bot_message_image('https://spectrogramma.cleverbots.ru/'+events[i].message.attachment);
                                    }, count_bot_message*500);

                            }
                            if (events[i].allowed_answer_type == "audio") {
                                cb_bot.bot_audio_microphone();
                                $('.js_bot_message_form').fadeOut(400);
                            }

                            if (events[i].allowed_answer_type == "email") {
                                $('.js_bot_message_form').fadeIn(400);
                                cb_bot.show_email_block();
                            }

                            if (events[i].message != undefined){
                                if (events[i].message.buttons != undefined) {
                                    setTimeout((e) => {
                                         cb_bot.show_buttons(events[i].message.buttons);
                                    },  count_bot_message*500);
                                }
                            }

                            count_bot_message++;
                            if (events[i].allowed_answer_type == "audio") { no_scroll = true;}
                        }
                        dialogue_id = response_server.dialogue_id;
                        if (!no_scroll) {
                            setTimeout(() => cb_bot.scroll_bottom(), 300);
                        }

                    }
                }
            };
        }
        show_buttons(buttons){
            let add_el = document.createElement('div');
            add_el.className = "cb_block_message_wrap cb_block_message_wrap_buttons";
            for (var i = 0; i < buttons.length; i++) {
                let button_el = document.createElement('div');
                button_el.className = "cb_btn js_cb_btn";

                let button_el__inner = document.createElement('span');
                button_el__inner.className = 'btn_cb_bot__inner';
                button_el__inner.innerHTML = buttons[i].text;;

                button_el.append(button_el__inner);
                    button_el.addEventListener('click', function () {
                        let bottom_textfield = $('.widget_mozg_bottom');
                        if (bottom_textfield.is(":hidden")) {
                            bottom_textfield.fadeIn(400);
                        }
                       cb_bot.send_server(button_el.textContent,'text')
                   });
                add_el.append(button_el);
            }
            $('.widget_mozg_main_conversation').append(add_el);
        }
        show_email_block(){
            let add_el = document.createElement('div');
            add_el.className = "cb_block_message_wrap cb_block_message_wrap_end_test";
            add_el.innerHTML = '<form action="javascript:void(0)" class="js_cb_send_form"><div class="cb_block_message_end_test_wrap"><div class="cb_block_message_end_test_text_field"><input type="text" required name="email" placeholder="Ваш e-mail" class="cb_block_message_input_field"><div class="cb_block_message_input_field_error_text">Упс! Кажется, вы немного ошиблись в адресе электронной почты. Пожалуйста, проверьте его.</div></div><button class="cb_block_message_end_test_send">Хочу получить результаты</button></div> </form>';
            $('.widget_mozg_main_conversation').append(add_el);




                document.querySelectorAll('.js_cb_send_form').forEach(function (item, idx) {
                    item.addEventListener('submit', function (){
                        var email = $('.cb_block_message_input_field').val();
                        if (!(cb_bot.validateEmail(email))) {
                            $('.cb_block_message_input_field_error_text').addClass('cb_block_message_input_field_error_text_show');
                            $('.cb_block_message_input_field').addClass('cb_block_message_input_field_error');
                            return false;
                        }
                        else {
                            $('.cb_block_message_input_field_error_text').removeClass('cb_block_message_input_field_error_text_show');
                            $('.cb_block_message_input_field').removeClass('cb_block_message_input_field_error');
                            $('.widget_mozg_bottom').fadeOut(400);
                            cb_bot.send_server(email,'text');
                            cb_bot.email_send();
                            return true;
                        }
                    })
                })

        }
        email_send(){
            let add_el = document.createElement('div');
            add_el.className = "cb_block_message_end_test_success";
            add_el.innerHTML = `<img src="${IMGSRC}check.svg" alt=""> <span>Отправлено</span>`;
            $('.widget_mozg_main_conversation').append(add_el);
        }

        bot_audio_microphone(){
            let add_el = document.createElement('div');
            add_el.className = "cb_block_recorder_block";


            let micro_el = document.createElement('div');
            micro_el.className = "cb_block_recorder_el";
            micro_el.innerHTML = `
                <div class="js_cb_block_recorder_btn js_start_audio cb_block_recorder_btn" tabindex="1">
                    <img src="${IMGSRC}Microphone.svg">
                    <div class="cb_block_recorder_btn_recordering">
                        <div class="cb_block_recorder_btn_recordering_image"></div>
                        <div class="cb_block_recorder_btn_recordering_text">00:00</div>
                    </div>
                </div>

                <div class="cb_block_recorder_add_text js_cb_block_recorder_btn_text">ЗАПИСАТЬ ОТВЕТ РЕБЁНКА</div>
                <div class="cb_block_recorder_add_text_recordering">Нажмите, чтобы остановить запись</div>
                `;
            add_el.append(micro_el);
            $('.widget_mozg_main_conversation').append(add_el);
            let btn_pl = document.querySelectorAll('.js_bot_started>div');
            btn_pl = btn_pl[btn_pl.length- 1];

            // setTimeout(function(){btn_pl.querySelector('.js_cb_block_recorder_btn').focus({preventScroll:true});},200);


            document.querySelectorAll('.js_cb_block_recorder_btn_text').forEach(function (item, idx) {
               item.addEventListener('click', function (){
                    cb_bot.audio_work(item.previousSibling)
                })
            });
            document.querySelectorAll('.js_cb_block_recorder_btn').forEach(function (item, idx) {
               item.addEventListener('click', function (){
                    console.log(item);

                cb_bot.audio_work(item);
            });

               item.onkeydown = function(evt) {
                    evt = evt || window.event;
                    if (evt.keyCode==13) {
                        cb_bot.audio_work(item);
                    }
                };
            });

            setTimeout(() => cb_bot.scroll_bottom(), 300);
        }
        start_timer(){
            var totalSeconds = 0;
            timer_ = setInterval(setTime, 1000);

            function setTime()
            {
                ++totalSeconds;
                $('.cb_block_recorder_btn_recordering_text').text(pad(parseInt(totalSeconds/60)) + ':'+pad(totalSeconds%60) );
            }

            function pad(val)
            {
                var valString = val + "";
                if(valString.length < 2)
                {
                    return "0" + valString;
                }
                else
                {
                    return valString;
                }
            }
        }
        stop_timer(){
            clearTimeout(timer_);
        }
        audio_work(this_){


             if (this_.classList.contains('js_start_audio')) {
                  cb_bot.start_timer();
                  this_.classList.remove("js_start_audio");
                  this_.classList.add("recording_audio");
                  let el = this_.parentElement;
                  el.querySelector('.cb_block_recorder_add_text').classList.add('hide');
                  el.querySelector('.cb_block_recorder_add_text_recordering').classList.add('show');
                  cb_bot.audio_record_start();
              }
              else{
                  if (this_.classList.contains('recording_audio')) {
                      cb_bot.stop_timer();
                      this_.classList.remove("recording_audio");
                      this_.classList.add("audio_dissable");
                      let el = this_.parentElement;
                      el.querySelector('.cb_block_recorder_add_text_recordering').classList.remove('show');
                      cb_bot.audio_record_stop();
                      document.querySelectorAll('.js_cb_block_recorder_btn').forEach(function (item, idx) {
                           item.removeEventListener('click', cb_bot.audio_work);
                      });
                      $('.js_cb_block_recorder_btn').fadeOut(400);

                  }
              }


        }

        bot_message_user(text){


            $('.widget_mozg_main_conversation').append(`
                <div class="cb_block_message_wrap cb_block_message_wrap_user">
                    <div class="cb_block_message cb_block_message_user">${text}</div>
                    <img src="${IMGSRC}userIcon.svg" class="cb_block_message_user__icon">
                </div>
            `);

        }

        bot_message_image(src){
            $('.widget_mozg_main_conversation').append(`
                <div class="cb_block_message_wrap cb_block_message_wrap_with_image">
                    <div class="cb_block_message">
                        <div class="cb_block_message__image__wrap">
                            <img src="${src}">
                        </div>
                        <div class="cb_block_message__image__wrap__text">
                            <svg width="22" height="22" viewBox="0 0 22 22">
                                <path id="Path_2" data-name="Path 2" d="M16,5A11,11,0,1,0,27,16,11,11,0,0,0,16,5Zm1.843,16.152a5.511,5.511,0,0,1-1.923,1.823,3.323,3.323,0,0,1-1.567.335c-1.421-.118-2.081-1.189-1.461-2.892l1.868-5.133c.4-1.108.012-1.445-.308-1.453q-.571-.011-1.365,1.07a.282.282,0,0,1-.46-.326,5.513,5.513,0,0,1,1.923-1.823,3.32,3.32,0,0,1,1.567-.335c1.421.118,2.081,1.188,1.461,2.892l-1.868,5.133c-.4,1.108-.012,1.444.308,1.453q.571.011,1.365-1.07a.282.282,0,0,1,.46.326Zm1.5-10.685a1.329,1.329,0,1,1-.794-1.7,1.329,1.329,0,0,1,.794,1.7Z" transform="translate(-5 -5)" opacity="0.3"/>
                            </svg>
                            <p>Дорогие родители, помогите, пожалуйста, малышу отправлять мне голосовые сообщения. С помощью них я могу выявить проблемы с речью на ранней стадии.</p>
                        </div>
                    </div>
                </div>
            `);
            this.scroll_bottom();

        }


        bot_message(text, avatar=false, time=false) {
            let add_el = document.createElement('div');
            add_el.className = "cb_block_message_wrap";

                add_el.classList.add("cb_block_message_wrap_with_avatar");
                add_el.innerHTML = `<img src="${IMGSRC}avatar.png" class="cb_block_message_bot_avatar">`;

            let timeText;

            if (time) {
                add_el.classList.add("cb_block_message_wrap_with_time");

                let today = new Date();
                timeText = ("0" + today.getHours()).slice(-2) + ":" + ("0" + today.getMinutes()).slice(-2);
            }


            let message = document.createElement('div');
            message.className = "cb_block_message";
            message.innerHTML = text;

            if (time) {
                message.innerHTML = message.innerHTML + '<div class="cb_block_message_bot_date">'+timeText+'</div>'
                add_el.append(message);
            }
            else{
                add_el.append(message);
            }

            $('.widget_mozg_main_conversation').append(add_el);
        }


        show_audio_message(audio,blob){

            let add_el = document.createElement('div');
            add_el.className = "cb_block_message_wrap cb_block_message_wrap_user";

            let message = document.createElement('div');
            message.className = "cb_block_message cb_block_message_user";

            let audio_el = document.createElement('audio');



            audio_el.src = audio;
            audio_el.muted = true;
            // audio_el.controls = true;



            let audio_player = document.createElement('div');
            audio_player.className = "cb_block_message_user_audio js_audio_n_"+ audio_item;


            let time_track = '0:00';
            audio_player.innerHTML = '<div class="cb_play_btn__wrap js_cb_play_btn"><img src="'+IMGSRC+'btn_play.svg" class="cb_play_btn"></div><div class="cb_block_message_user_audio_track"></div><div class="cb_block_message_user_audio_time">'+time_track+'</div>';
            var class_x = '.js_audio_n_'+audio_item;
            audio_player.dataset.number_player = audio_item;

            let audio_player_btns = document.createElement('div');
            audio_player_btns.className = 'cb_block_message_wrap cb_block_message_wrap_buttons cb_block_message_wrap_buttons_user';

            let btn_1 = document.createElement('div');
            btn_1.className = 'cb_btn cb_btn--gray js_cb_btn_record_new';

            let btn_1__inner = document.createElement('span');
            btn_1__inner.className = 'btn_cb_bot__inner';
            btn_1__inner.innerHTML = 'Записать заново';

            btn_1.append(btn_1__inner);

            let btn_2 = document.createElement('div');
            btn_2.className = 'cb_btn js_cb_btn_send';

            let btn_2__inner = document.createElement('span');
            btn_2__inner.className = 'btn_cb_bot__inner';
            btn_2__inner.innerHTML = 'Отправить';

            btn_2.append(btn_2__inner);

            btn_2.setAttribute("tabindex", "1");

            audio_player_btns.append(btn_1);
            audio_player_btns.append(btn_2);




            message.append(audio_el);


            message.append(audio_player);



            add_el.append(message);
            add_el.insertAdjacentHTML('beforeend', `<img src="${IMGSRC}userIcon.svg" class="cb_block_message_user__icon">`);

            $('.widget_mozg_main_conversation').append(add_el);
            $('.widget_mozg_main_conversation').append(audio_player_btns);

            let btn_rep = false;
            btn_2.focus({preventScroll:true});

            function hide_send(elem){
                $(elem.parentElement).find('.cb_btn').slideUp(400, function() {
                    $(this).parent().remove();
                });
                $(elem.parentElement).prev().prev().slideUp(400, function() {
                    $(this).remove();
                });
            }

            function send_info(el){
                cb_bot.send_server(blob,'attachment');
                setTimeout(() =>  hide_send(el),500);
                btn_rep = true;
                btn_2.blur();
            }

            btn_2.onkeydown = function(evt) {
                evt = evt || window.event;
                if (evt.keyCode==13) {
                    send_info(this);
                }
            };

            btn_2.addEventListener('click', function(){
                send_info(this);
            })
            btn_1.addEventListener('click', function(){
                 $(this.parentElement).find('.cb_btn').slideUp(400);
                 $(this.parentElement).prev().slideUp(400);

                // setTimeout(() =>
                //    ,function(){
                //         cb_bot.scroll_bottom();
                //     }),
                // 500);

                cb_bot.bot_audio_microphone();
                btn_rep = true;

            })
            if (btn_rep==false) {
                setTimeout(() => cb_bot.scroll_bottom(), 300);
            }


            if ($(window).width()<=450) {
                wave_height = 21;
            }
            else{
                wave_height = 23;
            }
            wavesurfer = WaveSurfer.create({
                container: audio_player.querySelector('.cb_block_message_user_audio_track'),
                waveColor: '#E56A4F',
                progressColor: '#ccc',
                height:wave_height,
                barWidth: 2,
                barMinHeight:2,
                barHeight: 4,
                barGap: 2,
                cursorWidth:0,
            });
            audio_array.push(wavesurfer);



            wavesurfer.load(audio);
            wavesurfer.on('ready', function () {
                let second = Math.ceil(wavesurfer.getDuration());
                if (second>=10)
                    second = '00:'+second
                else
                    second = '00:0'+second
                $(class_x).find('.cb_block_message_user_audio_time').text(second)
            });







            function audio_player_stop(el) {
                el.querySelector('.cb_play_btn').src= IMGSRC+"btn_play.svg";
                el.classList.remove('played');
            }
            wavesurfer.on('finish', function () {
                audio_player_stop($('.cb_block_message.played')[0])
            });

            audio_player.querySelector('.js_cb_play_btn').addEventListener('click', function(){
                audio_array[this.parentElement.dataset.number_player].playPause()
                let audio_player_block_inner = this.parentElement;
                let audio_player_block = this.parentElement.parentElement;
                let el_audio = this.parentElement.parentElement.querySelector('audio');

                if (!(audio_player_block.classList.contains('played'))) {
                    audio_player_block.classList.add('played');
                    audio_player_block_inner.querySelector('.cb_play_btn').src= IMGSRC+"stop.svg";
                }
                else{
                    el_audio.currentTime = 0;
                    audio_player_stop(audio_player_block);
                }
            })
             audio_item++;
        }
        validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        audio_record_start(){
           StartRecording()
        }
        audio_record_stop(){
           StopRecording()
        }
    }






    function _check_microphone() {
        navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => { _check_m(stream) })
    }

    function _check_m(stream) {
        AudioContext = window.AudioContext || window.webkitAudioContext;
         audioContext = new AudioContext();

        analyser = audioContext.createAnalyser();
        microphone_ = audioContext.createMediaStreamSource(stream);
        javascriptNode = audioContext.createScriptProcessor(2048, 1, 1);

        analyser.smoothingTimeConstant = 0.8;
        analyser.fftSize = 1024;

        microphone_.connect(analyser);
        analyser.connect(javascriptNode);
        javascriptNode.connect(audioContext.destination);

        javascriptNode.onaudioprocess = function() {
            var array = new Uint8Array(analyser.frequencyBinCount);
            analyser.getByteFrequencyData(array);
            var values = 0;

            var length = array.length;
            for (var i = 0; i < length; i++) {
                values += (array[i]);
            }
            var average = Math.round(values / length / 20);
            color_volume(average);
        }
    }


     function color_volume(volume){
		  let all_pids = $('.widget_mozg_main_desktop_hello_microphone_view span');
		  let elem_range = all_pids.slice(0, volume)
		  for (var i = 0; i < all_pids.length; i++) {
		    all_pids[i].classList.remove('active');
		  }
		  for (var i = 0; i < elem_range.length; i++) {
		    elem_range[i].classList.add('active');
		  }
     }

     function captureMicrophone(callback) {
         if(microphone) {
             callback(microphone);
             return;
         }
         if(typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
             alert('Ваш браузер не поддерживает работу с микрофоном.');

             if(!!navigator.getUserMedia) {
                 alert('Ваш браузер не поддерживает работу с микрофоном.');
             }
         }
         navigator.mediaDevices.getUserMedia({
             audio: true
         }).then(function(microphone) {
             callback(microphone);
         }).catch(function(error) {
             alert('Не возможно получить доступ к микрофону.');
             console.error(error);
         });
     }



function StartRecording() {
    if (!microphone) {
        captureMicrophone(function(mic) {
            microphone = mic;
            StartRecording()
        });
        return;
    }

    var options = {
        type: 'audio',
        numberOfAudioChannels: isEdge ? 1 : 2,
        checkForInactiveTracks: true,
        bufferSize: 16384
    };
    if(isSafari || isEdge) {
        options.recorderType = StereoAudioRecorder;
    }
    if(navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
        options.sampleRate = 48000;
    }
    if(isSafari) {
        options.sampleRate = 44100;
        options.bufferSize = 4096;
        options.numberOfAudioChannels = 2;
    }
    if(recorder) {
        recorder.destroy();
        recorder = null;
    }
    recorder = RecordRTC(microphone, options);
    recorder.startRecording();
}


function ReleaseMicrophone() {
    if(microphone) {
        microphone.stop();
        microphone = null;
    }
}


function StopRecording() {
    recorder.stopRecording(function(){
        let blob = recorder.getBlob();
        let blobURL = URL.createObjectURL(blob);
        console.log(blobURL);
        cb_bot.show_audio_message(blobURL,blob);

        if(isSafari) {
            ReleaseMicrophone()
        }
    });
}
$(function () {
    cb_bot = new cb();

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const spectrogram = urlParams.get('spectrogram')
    if  (spectrogram!=null){
        document.querySelector('.js_go_conversstion').click();
    }

});

